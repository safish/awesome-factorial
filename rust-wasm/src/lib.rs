use wasm_bindgen::{prelude::wasm_bindgen, JsValue, __rt::IntoJsResult};

struct BigInt{
  data: Vec<i32>
}

const STEP:i32 = 10000;
const STEP_LENGTH:i32 = 4;

impl BigInt {
  fn multiply(&mut self, m: i32) {
    let mut x = 0;
    // let mut data = &self.data;

    let mut i = 0;

    while i < self.data.len() {
      x += self.data[i] * m;
      self.data[i] = x % STEP;
      x /= STEP;
      i += 1;
    }

    while x > 0 {
      self.data.push(x % STEP);
      x /= STEP;
    }
  }

  fn to_string(&mut self) -> String {
    let mut list: Vec<String> = vec![];
    let data = &self.data;
    let last = data.len() - 1;
    let mut i = last;
    let zero = 0usize;
    while i >= zero  {
      let c = data[i].to_string();
      if i < last {
        let a = STEP_LENGTH - i32::try_from(c.len()).unwrap();
        list.push("0".repeat(a.try_into().unwrap()));
      }
      list.push(c);

      if i == 0 {
        break;
      } else {
        i -= 1;
      }
    }
    list.join("")
  }
}

#[wasm_bindgen]
pub fn factorial(n: i32) -> Box<[JsValue]> {
  // let now = std::time::Instant::now();
  let w = web_sys::window().unwrap();

  let performance = w.performance().unwrap();

  let now = performance.now();

  let mut r = BigInt{data: vec![1]};

  let mut i = 2;

  while i <= n {
    r.multiply(i);
    i += 1;
  }

  Box::new([r.to_string().into_js_result().unwrap(), (performance.now() - now).into_js_result().unwrap()])
}
