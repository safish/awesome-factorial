class LargeNumber {
  size = 256;
  step = 10000;

  constructor() {
    this.data = new Uint32Array(this.size);
    this.length = 1;
  }

  enlarge() {
    this.size *= 2;
    const data = new Uint32Array(this.size);
    data.set(this.data);
    this.data = data;
  }

  multiplyBy(m) {
    let x = 0;
    for (let i = 0; i < this.length; i += 1) {
      x += this.data[i] * m;
      this.data[i] = x % this.step;
      x = Math.floor(x / this.step);
    }
    while (x > 0) {
      if (this.length >= this.size) this.enlarge();
      this.data[this.length] = x % this.step;
      x = Math.floor(x / this.step);
      this.length += 1;
    }
  }

  toString() {
    const data = Array.from({ length: this.length }, (_, i) => this.data[i]);
    const first = data.pop();
    return first + data.reverse().map(i => `${i + this.step}`.slice(1)).join('');
  }
}

export function factorial(n) {
  const startTime = performance.now();
  const result = new LargeNumber();
  result.data[0] = 1;
  for (let i = 1; i <= n; i += 1) {
    result.multiplyBy(i);
  }
  return [result.toString(), performance.now() - startTime];
}
