DIST=$(CURDIR)/dist
export DIST

all: go-wasm ocaml-js ocaml-melange rust-wasm

go-wasm:
	$(MAKE) -C go-wasm

rust-wasm:
	$(MAKE) -C rust-wasm

ocaml-js:
	$(MAKE) -C ocaml-js

ocaml-melange:
	$(MAKE) -C ocaml-melange

clean:
	@rm -rf $(DIST)
	$(MAKE) -C go-wasm clean
	$(MAKE) -C ocaml-js clean
	$(MAKE) -C ocaml-melange clean

dist:
	mkdir -p $(DIST)/js
	cp index.html index.js $(DIST)
	cp -r js/* $(DIST)/js
	$(MAKE) -C go-wasm dist
	$(MAKE) -C rust-wasm dist
	$(MAKE) -C ocaml-js dist
	$(MAKE) -C ocaml-melange dist

deploy: dist
	./deploy.sh

.PHONY: go-wasm rust-wasm ocaml-js ocaml-melange clean dist
