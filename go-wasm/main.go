package main

import (
	"strconv"
	"strings"
	"syscall/js"
	"time"
)

const step = 10000
const stepLen = 4

type bigNumber struct {
	data *[]int
}

func (bn *bigNumber) multiplyBy(m int) {
	x := 0
	data := *bn.data
	for i := 0; i < len(data); i++ {
		x += data[i] * m
		data[i] = x % step
		x /= step
	}
	for x > 0 {
		data = append(data, x%step)
		x /= step
	}
	bn.data = &data
}

func (bn *bigNumber) toString() string {
	var s strings.Builder
	data := *bn.data
	last := len(data) - 1
	for i := last; i >= 0; i-- {
		c := strconv.Itoa(data[i])
		if i < last {
			s.WriteString(strings.Repeat("0", stepLen-len(c)))
		}
		s.WriteString(c)
	}
	return s.String()
}

func factorial(n int) (string, float64) {
	start := time.Now()
	r := bigNumber{&[]int{1}}
	for i := 2; i <= n; i++ {
		r.multiplyBy(i)
	}
	result := r.toString()
	elapsed := time.Since(start)
	return result, elapsed.Seconds()
}

func factorialWrapper() js.Func {
	factorialFn := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		n := args[0].Int()
		result, time := factorial(n)
		return []interface{}{result, time * 1000}
	})
	return factorialFn
}

func main() {
	js.Global().Set("factorial.go", factorialWrapper())
	<-make(chan bool)
}
