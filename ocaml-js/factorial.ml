type bignumber = { mutable data : int array; mutable size : int };;

let step_size = 4 in
let step =
  let rec step_calc r n = if n > 0 then step_calc (r * 10) (n - 1) else r in
  step_calc 1 step_size
in
let init_array =
  let data = Array.make 256 0 in
  data.(0) <- 1;
  data
in
let create_one () = { data = Array.copy init_array; size = 1 } in
let enlarge r =
  let len = Array.length r.data in
  let d = Array.make (len * 2) 0 in
  Array.blit r.data 0 d 0 len;
  r.data <- d;
  r
in
let mult r n =
  let x = ref 0 in
  for i = 0 to r.size - 1 do
    x := !x + (n * r.data.(i));
    r.data.(i) <- !x mod step;
    x := !x / step
  done;
  while !x > 0 do
    let r = if Array.length r.data <= r.size then enlarge r else r in
    r.data.(r.size) <- !x mod step;
    x := !x / step;
    r.size <- r.size + 1
  done;
  r
in
let fact n =
  let rec fact_aux r n = if n > 1 then fact_aux (mult r n) (n - 1) else r in
  fact_aux (create_one ()) n
in
let leftpad l s =
  let len = String.length s in
  if len < l then String.make (l - len) '0' ^ s else s
in
let rev_map f l =
  let rec rev_map_aux r l i =
    match l with
    | [] -> r
    | [ hd ] -> f hd i :: r
    | hd :: rest -> rev_map_aux (f hd i :: r) rest (i - 1)
  in
  rev_map_aux [] l (List.length l - 1)
in
let repr r =
  Array.sub r.data 0 r.size |> Array.to_list
  |> rev_map (fun x i ->
         let x = string_of_int x in
         if i > 0 then leftpad step_size x else x)
  |> String.concat ""
in
let open Js_of_ocaml in
let now () =
  let js_date = new%js Js.date_now in
  js_date##valueOf
in
let fact_repr x =
  let start_time = now () in
  let out = x |> fact |> repr in
  let elapsed = now () -. start_time in
  (out, elapsed)
in
(*
let (res, t) = fact_repr 10 in
Printf.printf "%s %F\n%!" res t
*)
Js.export "factorial.ocaml" fact_repr
