import {
  createApp,
  ref,
} from 'https://cdn.jsdelivr.net/npm/vue@3/dist/vue.esm-browser.prod.js';

function timeit(name, times, fn, ...args) {
  const startTime = performance.now();
  const output = { name, selfTime: 0 };
  for (let i = 0; i < times; i++) {
    const [res, dt] = fn(...args);
    output.selfTime += dt;
    output.result = res;
  }
  output.time = performance.now() - startTime;
  return output;
}

const loaders = {
  async bigInt() {
    const { factorial } = await import('./js/big-int.js');
    return factorial;
  },
  async typedArray() {
    const { factorial } = await import('./js/typed-array.js');
    return factorial;
  },
  async goWasm() {
    await import('./go-wasm/wasm_exec.js');
    const go = new window.Go();
    const result = await WebAssembly.instantiateStreaming(
      fetch('go-wasm/main.wasm'),
      go.importObject,
    );
    go.run(result.instance);
    return window['factorial.go'];
  },
  async ocamlJs() {
    await import('./ocaml-js/factorial.bc.js');
    const fn = window['factorial.ocaml'];
    return (...args) => {
      const [code, ...rest] = fn(...args);
      if (code) throw code;
      return rest;
    };
  },
  async ocamlMelange() {
    const { factorial } = await import('./ocaml-melange/factorial.js');
    return factorial;
  },
  async rustWasm() {
    const { factorial, default: init } = await import('./rust-wasm/bigint.js');
    await init();
    return factorial;
  },
};
Object.keys(loaders).forEach((key) => {
  const value = loaders[key];
  loaders[key] = (cache => () => cache)(value());
});

createApp({
  setup() {
    const input = ref(2000);
    const times = ref(20);
    const calculating = ref(false);
    const output = ref('');
    const results = ref([]);

    const calculate = async (input, times) => {
      output.value = '';
      results.value = [];
      let expected;
      for (const args of [
        ['Native BigInt', times, await loaders.bigInt(), input],
        ['JS', times, await loaders.typedArray(), input],
        ['Go WASM', times, await loaders.goWasm(), input],
        ['Rust WASM', times, await loaders.rustWasm(), input],
        ['OCaml JS', times, await loaders.ocamlJs(), input],
        ['OCaml Melange', times, await loaders.ocamlMelange(), input],
      ]) {
        await new Promise(requestAnimationFrame);
        const { name, time, selfTime, result } = timeit(...args);
        if (!expected) {
          expected = result;
          output.value = `${input}! = ${expected}`;
        }
        const newValue = [
          ...results.value,
        ];
        const newItem = {
          name,
          time,
          selfTime,
          ok: result === expected,
        };
        let i = newValue.findIndex(item => item.time > time);
        if (i < 0) i = newValue.length;
        newValue.splice(i, 0, newItem);
        results.value = newValue;
      }
    };

    const onSubmit = () => {
      if (calculating.value) return;
      calculating.value = true;
      calculate(input.value, times.value).finally(() => {
        calculating.value = false;
      });
    }

    return { input, times, calculating, output, results, onSubmit };
  },
}).mount('#app');
